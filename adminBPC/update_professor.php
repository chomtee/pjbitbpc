<?php include_once("loginDB.php");?>
<html lang="en">
<head>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เพิ่ม/แก้ไข ข้อมูลคณะอาจารย์</title>
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/update_pro.css">

</head>
<body>
<form action="loginDB.php" method="POST">

    <div class="head-con">
        <div class="nav_logo">
            <img src="../adminBPC/pic/Image 12224.png" alt="" >
       </div>

        <div class="nav_logout">
            <div class="logout">
                <h1>User :
                    <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
                </h1>
            </div>
        </div>
        <div class="menu">
            <div class="bg-menu">
                <h1>จัดการเว็บไซต์</h1>
                        <hr>
                        <ul class="ul-menu">
                            <a href="home.php"><li > หน้าหลักเว็บไซต์</li></a>
                           <li class="showli"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                                <a href="professor.php"><li class="active">คณะอาจารย์</li></a>
                                <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                            </ul>
                            <a href="Yearbook.php"><li>ทำเนียบรุ่น</li></a>
                            <a href="activity.php"><li> ภาพกิจกรรม</li></a>
                            <a href="QA.php"><li >คำถาม QA</li></a>
                            <a href="contact.php"><li>การติดต่อ</li></a>
                           <a href="massage.php"><li>กล่องข้อความ</li></a>
                            <a href="update_admin.php"><li>Admin</li></a>
                          </ul>
            </div>
        </div>
        <!-- end head -->

         <!-- start content-->
        <div class="content">
            <div class="head-text">
                <h1> เพิ่ม/แก้ไข ข้อมูลคณะอาจารย์</h1>
            </div>
            <div class="content-inside">
                <form action="insert_update_professor.php" method="POST" enctype="multipart/form-data">
             
                <div class="previewimg" style="margin-top: 16%;"></div> 
                <div class="gbtn">
                    <input type="file" name="upload_professor" class="btn-upload">
                    <p class='textred' style='color:red'>ต้องใส่รูปภาพอย่างน้อย 1 รูปภาพ</p>
                </div>
                
                <div class="content-tech" style="margin-top: -16%;">
                    <label for="">ชื่ออาจารย์ : </label> <input type="text" name="name" id="" placeholder="กรุณากรอกข้อมูล"><br>
                    <label for="">ตำแหน่ง : </label> <select name="major" id=""><br>
                        <option value="หัวหน้าสาขาเทคโนโลยีสารสนเทศทางธุรกิจ">หัวหน้าสาขาวิชาเทคโนโลยีสารสนเทศทางธุรกิจ</option>
                        <option value="อาจารย์ประจำสาขาวิชาเทคโนโลยีสารสนเทศทางธุรกิจ">อาจารย์ประจำสาขาวิชาเทคโนโลยีสารสนเทศทางธุรกิจ</option>
                    </select><br>
                    <label for="">ความรับผิดชอบ : </label> <select name="responsible" id=""><br>
                        <option value="ผู้รับผิดชอบหลักสูตร">ผู้รับผิดชอบหลักสูตร</option>
                        <option value="-">-</option>
                    </select><br>
                    <label for="">Email : </label><input type="text" name="email" id=""placeholder="E-mail"><br>
                    <label for="">เบอร์โทรศัพท์ : </label><input type="text" name="number" id=""placeholder="xxx-xxx-xxxx">
                    
                    
                </div>
               
                    <div class="gb-btn">
                            <input type="submit" value="เพิ่ม/บันทึก" name="btn" class="btn-ok">
                            <a href="professor.php"><input type="button" value="ย้อนกลับ" class="btn-back"></a>
                    </div>
            </div>
            </form>
        </div>
        <!-- <footer>
            <div class="foot">
            </div>
        </footer> -->
</body>
</html>