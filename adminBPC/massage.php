<?php include_once("loginDB.php");?>
<?php 
        $query=mysqli_query($conn,"SELECT COUNT(ID) FROM `tbcontact`");
        $row = mysqli_fetch_row($query);
      
        $rows = $row[0];
      
        $page_rows = 8;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 5 record / หน้า 
      
        $last = ceil($rows/$page_rows);
      
        if($last < 1){
          $last = 1;
        }
      
        $pagenum = 1;
      
        if(isset($_GET['pn'])){
          $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
        }
      
        if ($pagenum < 1) {
          $pagenum = 1;
        }
        else if ($pagenum > $last) {
          $pagenum = $last;
        }
      
        $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
      
        $nquery=mysqli_query($conn,"SELECT * from  tbcontact $limit");
      
        $paginationCtrls = '';
      
        if($last != 1){
      
        if ($pagenum > 1) {
      $previous = $pagenum - 1;
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="btn btn-info">Previous</a> &nbsp; &nbsp; ';
      
          for($i = $pagenum-4; $i < $pagenum; $i++){
            if($i > 0){
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary">'.$i.'</a> &nbsp; ';
            }
        }
      }
      
        $paginationCtrls .= ''.$pagenum.' &nbsp; ';
      
        for($i = $pagenum+1; $i <= $last; $i++){
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary">'.$i.'</a> &nbsp; ';
          if($i >= $pagenum+4){
            break;
          }
        }
      
      if ($pagenum != $last) {
      $next = $pagenum + 1;
      $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="btn btn-info">Next</a> ';
      }
        }
  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>กล่องข้อความ</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/massage.css">
</head>
<body>
<form action="loginDB.php" method="POST">

    <div class="head-con">
        <div class="nav_logo">
          <img src="../adminBPC/pic/Image 12224.png" alt="" >
       </div>

        <div class="nav_logout">
            <div class="logout">
                <h1>User :
                    <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
                    </form>
                </h1>
            </div>
        </div>
        <div class="menu">
            <div class="bg-menu">
                <h1>จัดการเว็บไซต์</h1>
                        <hr>
                        <ul class="ul-menu">
                            <a href="home.php"><li > หน้าหลักเว็บไซต์</li></a>
                           <li class="showli"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                                <a href="professor.php"><li>คณะอาจารย์</li></a>
                                <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                            </ul>
                            <a href="Yearbook.php"><li >ทำเนียบรุ่น</li></a>
                            <a href="activity.php"><li> ภาพกิจกรรม</li></a>
                            <a href="QA.php"><li >คำถาม QA</li></a>
                            <a href="contact.php"><li>การติดต่อ</li></a>
                           <a href="massage.php"><li class="active">กล่องข้อความ</li></a>
                            <a href="update_admin.php"><li>Admin</li></a>
                          </ul>
            </div>
        </div>
        <!-- start content -->
        
        <div class="content">
              
        <table>
           
            <tr  >
                <td colspan="6" style="padding: 0.5%;
                color: white;
               font-size: 30px;
               background: #A03838;
               border: 1px solid #707070;">กล่องข้อความ </td> 
            </tr>
            <tr class="head-table">
                 <td style="width: 20%; height: 65px;">ชื่อ</td>
                 <td style="width: 50%; height: 65px;">ข้อความ</td>
                 <td style="width: 20%;">ลบ</td>
            </tr>
            <form action="update_massage.php" method="POST">
            <?php
                    include_once("connectDB.php");
                    // $query = "SELECT * FROM tbcontact ";
                    // $result = mysqli_query($conn,$query);
                     //    while($rs = mysqli_fetch_array($result))
                while($rs = mysqli_fetch_array($nquery)) //อันใหม่ส่งผลถึง ตัดหน้า
                   {
            echo "<tr class='detall-massage'>";
                echo      "<td>$rs[ne]</td>";
                echo    "<td>$rs[tex]</td>";
                echo "<td><button class='btn-delete' type='submit' name='delete'id='open-popup-btn' value='$rs[ID]'onClick=\"return confirm('Are you sure you want to delete??');\">ลบ</button></td>";    
             echo "</tr>";}?>
             </form>
            
       

        </table>
        <div id="pagination_controls"><?php echo $paginationCtrls; ?></div>       



    </div>
    <footer>
        <div class="foot">
        </div>
    </footer>
    <script>

    </script>
</body>
</html>