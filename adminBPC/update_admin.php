<?php include_once("loginDB.php");
include_once("changpass.php");
session_start();?>
<html lang="en">
<head>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/update_admin.css">
    <title>เพิ่ม/แก้ไข ผู้ดูแลระบบ</title>
</head>
<body>
<form action="loginDB.php" method="POST">

    <div class="head-con">
        <div class="nav_logo">
            <img src="../adminBPC/pic/Image 12224.png" alt="" >
       </div>

        <div class="nav_logout">
            <div class="logout">
                <h1>User :
                    <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
                </h1>
            </div>
        </div>
        <div class="menu">
            <div class="bg-menu">
                <h1>จัดการเว็บไซต์</h1>
                        <hr>
                        <ul class="ul-menu">
                            <a href="home.php"><li > หน้าหลักเว็บไซต์</li></a>
                           <li class="showli"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                                <a href="professor.php"><li>คณะอาจารย์</li></a>
                                <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                            </ul>
                            <a href="Yearbook.php"><li >ทำเนียบรุ่น</li></a>
                            <a href="activity.php"><li> ภาพกิจกรรม</li></a>
                            <a href="QA.php"><li >คำถาม QA</li></a>
                            <a href="contact.php"><li>การติดต่อ</li></a>
                            <a href="massage.php"><li>กล่องข้อความ</li></a>
                            <a href="update_admin.php"><li class="active">Admin</li></a>
                          </ul>
            </div>
        </div>
        <!-- end head -->
        <div class="content">
            <div class="head-text">
                <h1>แก้ไขข้อมูล Admin</h1>
            </div>
           <form action="changpass.php" method="POST">
            <div class="content-inside">
                <div class="grid_content">
                    <div class="grid_item1">
                        <fieldset>
                            <legend>สำหรับแก้ไขข้อมูล</legend>
                        
                            <p class="nameAd">ชื่อ Admin : <input type="text" name="nameAdmin" id="name_admin" value=<?php echo "'$_SESSION[username]'"?>></p>
                            <p class="oldpass">รหัสผ่านเก่า  : <input type="password" name="oldpass" id="oldpass"></p>
                            <p class="newpass">รหัสผ่านใหม่ : <input type="password" name="newpass1" id="newpass"></p>
                            <p class="newpass">ยืนยันรหัสผ่าน : <input type="password" name="newpass2" id="newpass"></p>
                            <button class="btn-ok" type="submit" name="changpass">บันทึก</button>
                            </fieldset>
                    </div>
                    <div class="grid_item2">
                        <fieldset>
                            <legend>สำหรับการเพิ่มสมาชิก</legend>
                        
                            <p class="nameAd">ชื่อ Admin : <input type="text" name="add-admin" id="name_admin" placeholder="Admin"></p>
                            <p class="newpass">รหัสผ่านใหม่ : <input type="password" name="add-password1" id="newpass"></p>
                            <p class="newpass">ยืนยันรหัสผ่าน : <input type="password" name="add-password2" id="newpass"></p>
                            <?php if(isset($_SESSION['passerror'])||isset($_SESSION['usererror'])):?>
                                <span style="color: red;"><?php echo $_SESSION['passerror'].","?></span>
                                <span style="color: red;"><?php echo $_SESSION['usererror']."<br>"?></span>
                                <?php endif;?>
                                
                                <label for="">ความรับผิดชอบ : </label> 
                                
                                <select name="major" id="">
                                
                                    <option value="admin">หัวหน้าผู้ดูเเล</option>
                                    <option value="member">ผู้ดูเเล</option>
                                </select>
                            <button class="btn-upload" type="submit" name="register">เพิ่ม</button><br>
                        </fieldset>
                    </div>
            
                </div> 
                <!-- end grid -->
                <hr class="end_centent">
                <?php include_once('connectDB.php');
                
                        $query="SELECT * FROM tbadmin";
                        $result=mysqli_query($conn,$query);
                if($result){
                    
           
               
                   if($_SESSION['status']=="admin"){
                    
                       echo "<table class='table'>
                       <thead class='thead-light'>
                       <tr>
                       <th scope='col'>#</th>
                       <th scope='col'style='width:60%'>ชื่อ</th>
                       <th scope='col' style='width:20%'>ลบ</th>
                       </tr>
                   </thead>";
                       while($rs = mysqli_fetch_array($result)){
                    echo " 
                       
                    <tbody>
                        <tr>
                        <th scope='row'>$rs[ID]</th>
                        <td>$rs[Name]</td>
                        <td><button class='btn-delete' type='submit' name='delete' value='$rs[ID]' onClick=\"return confirm('คุณแน่ใจใช่หรือไม่ที่จะลบข้อมูลทั้งหมดของบัญชีนี้ $rs[Name]');\">ลบ</button></td>
                        </tr>
                    ";}
                    echo "</tbody>
                    </table>";
            }
                }else{
                    echo "error";
                }?>
               
                                </div>
            </form>
        </div>

      
</body>
</html>