
<?php                   include_once('loginDB.php');
                        include_once('connectDB.php');
        $query=mysqli_query($conn,"SELECT COUNT(Year) FROM `tbyear` WHERE Status=1");
        $row = mysqli_fetch_row($query);
       
      
        $rows = $row[0];
      
        $page_rows = 5;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 5 record / หน้า 
        
        $last = ceil($rows/$page_rows);
     
        if($last < 1){
          $last = 1;
        }
      
        $pagenum = 1;
      
        if(isset($_GET['pn'])){
          $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
        }
      
        if ($pagenum < 1) {
          $pagenum = 1;
        }
        else if ($pagenum > $last) {
          $pagenum = $last;
        }
      
        $limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;
        

        $nquery=mysqli_query($conn,"SELECT * from  tbyear  
                                            
                                             order by Year DESC
                                             
                                               $limit");

        
        
      
        $paginationCtrls = '';
      
        if($last != 1){
      
        if ($pagenum > 1) {
      $previous = $pagenum - 1;
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="btn btn-info">Previous</a> &nbsp; &nbsp; ';
      
          for($i = $pagenum-4; $i < $pagenum; $i++){
            if($i > 0){
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary">'.$i.'</a> &nbsp; ';
            }
        }
      }
      
        $paginationCtrls .= ''.$pagenum.' &nbsp; ';
      
        for($i = $pagenum+1; $i <= $last; $i++){
          $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary">'.$i.'</a> &nbsp; ';
          if($i >= $pagenum+4){
            break;
          }
        }
      
      if ($pagenum != $last) {
      $next = $pagenum + 1;
      $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="btn btn-info">Next</a> ';
      }
        }
  ?>
<html lang="en">
<head>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ทำเนียบรุ่น</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/yearbook.css">
</head>
<body> 
<form action="loginDB.php" method="POST">

    <div class="head-con">
    <div class="nav_logo">
        <img src="../adminBPC/pic/Image 12224.png" alt="" >
   </div>

    <div class="nav_logout">
        <div class="logout">
            <h1>User :
                <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
            </h1>
        </div>
    </div>
    <div class="menu">
        <div class="bg-menu">
            <h1>จัดการเว็บไซต์</h1>
                    <hr>
                    <ul class="ul-menu">
                      <a href="home.php"><li > หน้าหลักเว็บไซต์</li></a>
                     <li class="showli"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                          <a href="professor.php"><li>คณะอาจารย์</li></a>
                          <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                      </ul>
                      <a href="Yearbook.php"><li class="active">ทำเนียบรุ่น</li></a>
                      <a href="activity.php"><li> ภาพกิจกรรม</li></a>
                      <a href="QA.php"><li >คำถาม QA</li></a>
                      <a href="contact.php"><li>การติดต่อ</li></a>
                      <a href="massage.php"><li>กล่องข้อความ</li></a>
                      <a href="update_admin.php"><li>Admin</li></a>
                    </ul>
        </div>
    </div>
    <div class="content">
        <div class="head-text">
            <h1>ทำเนียบรุ่น</h1>
        </div>
        <div class="content-inside">
          <div class="headA">
          <a href="yearbook_update.php"><input type="button" value="เพิ่มรุ่นนักศึกษา" class="btn-add"></a></div>

<!-- table -->
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col"style="text-align: center;">รุ่น</th>
        <th scope="col" style="text-align: center;">จำนวน</th>
        <th scope="col"style="">รูป</th>
        <th scope="col"style="text-align: center;">สถานะ</th>
        <th scope="col"style="text-align: center;">แก้ไข</th>
        <th scope="col"style="text-align: center;">ลบ</th>
        <th scope="col"style="text-align: center;">เพิ่ม</th>
      </tr>
    </thead>
    <tbody class="content-table">
<form action="insert_update_yearbook.php" method="POST">
    <?php 
    while($rs = mysqli_fetch_array($nquery)){
     echo " <tr>
        <td scope='row' >$rs[Year] </td >
        <td >$rs[Member]</td>
        <td ><img src='../adminBPC/upload_yearbook/$rs[Yearimg]' alt='' class='photo-new'></td>";
        
        if($rs["Status"]== 1){
          echo "<td><img src='../adminBPC/pic/icons8-checkmark-64.png' alt=''></td>
          ";}else{
           echo "<td ><img src='../adminBPC/pic/icons8-delete-64.png' alt=''></td>
           ";
          }
          echo"
        <td ><button type='submit' class='btn-ok' name='updateYear' value='$rs[Year]'>แก้ไข</button></td>
        <td ><button class='btn-delete' name='delete' value='$rs[Year]' onClick=\"return confirm('คุณแน่ใจใช่หรือไม่ที่จะลบข้อมูลทั้งหมดของรุ่น $rs[Year]');\">ลบ</button></td>";
        $queryIf="SELECT * FROM tbstu WHERE year ='$rs[Year]'";
        $resultIf=mysqli_query($conn,$queryIf);
        echo "<td >";
        if(mysqli_num_rows($resultIf)!=$rs['Member']){
        echo"
        <button  type='submit' name='insertStudent' class='btn-upload' value='$rs[Year]'>เพิ่มนักศึกษา</button><br>";
        }
       
        if(mysqli_num_rows($resultIf)!=0){
        echo "
        <button  type='submit' name='updateStudent' class='btn-upload2' value='$rs[Year]'>แก้ไขนักศึกษา</button></td>
      </tr>";
    }
    }?>
 </form>
   <!-- <tr>
        <th scope="row" >14 </td>
        <td >xx คน</td>
        <td ><img src="https://via.placeholder.com/190x150" alt="" class="photo-new"></td>
     
        <td ><a href="yearbook_detail.php"><button class="btn-ok" value="edit">แก้ไข</button></a></td>
        <td ><button class="btn-delete" value="delete">ลบ</button></td>
        <td ><button class="btn-upload" value="upload">เพิ่มนักศึกษา</button></td>
      </tr>
      <tr>
        <th scope="row" style="">13</th>
        <td style="">xx คน</td>
        <td style=""><img src="https://via.placeholder.com/190x150" alt="" class="photo-new"></td>
        <td style=""><img src="/pic/icons8-checkmark-64.png" alt=""></td>
        <td style=""><a href="yearbook_detail.php"><button class="btn-ok" value="edit">แก้ไข</button></a></td>
        <td style=""><button class="btn-delete" value="delete">ลบ</button></td>
        <td style=""><button class="btn-upload" value="upload">เพิ่มนักศึกษา</button></td>
      </tr>  -->
    </tbody>
  </table>
  <div id="pagination_controlsa"><?php echo $paginationCtrls; ?></div>       

    <hr class="end_centent">
</div><!-- ************* End content 5 award *************-->
</div>

</body>
</html>