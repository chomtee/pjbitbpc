<?php include_once("loginDB.php");?>
<html lang="en">
<head>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>แก้ไข หลักสูตรระดับปริญญาตรี 4 ปี เทียบโอนรายวิชา</title>
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/update_major.css">
    <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

</head>
<body>
<form action="loginDB.php" method="POST">

    <div class="head-con">
        <div class="nav_logo">
            <img src="../adminBPC/pic/Image 12224.png" alt="" >
       </div>

        <div class="nav_logout">
            <div class="logout">
                <h1>User :
                    <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
                </h1>
            </div>
        </div>
        <div class="menu">
            <div class="bg-menu">
                <h1>จัดการเว็บไซต์</h1>
                        <hr>
                        <ul class="ul-menu">
                            <a href="home.php"><li > หน้าหลักเว็บไซต์</li></a>
                           <li class="showli"class="active"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                                <a href="professor.php"><li>คณะอาจารย์</li></a>
                                <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                            </ul>
                            <a href="Yearbook.php"><li >ทำเนียบรุ่น</li></a>
                            <a href="activity.php"><li > ภาพกิจกรรม</li></a>
                            <a href="QA.php"><li >คำถาม QA</li></a>
                            <a href="contact.php"><li>การติดต่อ</li></a>
                            <a href="massage.php"><li>กล่องข้อความ</li></a>
                            <a href="update_admin.php"><li>Admin</li></a>
                          </ul>
            </div>
        </div>
        <!-- End head -->
        <!-- Start Content -->
        <div class="content">
            <div class="head-text">
                <form action="insert_update_p4.php" method="POST">
                <h1>แก้ไข หลักสูตรระดับปริญญาตรี 4 ปี เทียบโอนรายวิชา</h1>
            </div>
            <div class="content-inside">
                <div class="text-head"> <span class="span-topic">หัวข้อ : </span> <input type="text" name="toppic" id="input-topic" placeholder="ชื่อเรื่อง..."><br><p>ความยาวไม่เกิน 50 ตัวอักษร <span></span>/50</p>
                <br>
                    <div class="status"><span  class="span-topic"> สถานะการเผยแพร่ : </span>  <input type="radio" name="status" id="radio-on" value="1"> <label for="radio-on">เผยแพร่</label> 
                        <input type="radio" name="status" id="radio-off" value="0"> <label for="radio-off">ไม่เผยแพร่</label><br>
                    </div> 
                   
               <div class="div-textarea"> <span  class="span-topic">รายละเอียด </span> <textarea name="description" id="" cols="20" rows="10"></textarea> </div> 
               <div class="Gbtn"> <input type="submit"name="btn-ok" value="เพิ่ม/บันทึก" class="btn-ok">
                <a href="activity.php"><input type="button" value="ย้อนกลับ" class="btn-back"></div></a>
                </div>

            </div>
            </form>
        </div>
    <footer>
        <div class="foot">
        </div>
    </footer>
    <script>
        CKEDITOR.replace( 'description' );
</script>
</body>
</html>