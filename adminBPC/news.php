<?php include_once("loginDB.php");?>
<html lang="en">
<head>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet">
    <title>เพิ่ม/แก้ไข ข่าวสาร</title>
    <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="../adminBPC/head_sideAndFooter.css">
    <link rel="stylesheet" href="../adminBPC/news.css">
</head>
<body>
<form action="loginDB.php" method="POST">

    <div class="head-con">
        <div class="nav_logo">
            <img src="../adminBPC/pic/Image 12224.png" alt="" >
       </div>

        <div class="nav_logout">
            <div class="logout">
                <h1>User :
                    <span id="nameuser"><?php echo $_SESSION['username'];?></span> 
                    <button class="btn-logout" type="submit" name="logout">Logout</button>
                    </form>
                </h1>
            </div>
        </div>
        <div class="menu">
            <div class="bg-menu">
                <h1>จัดการเว็บไซต์</h1>
                        <hr>
                        <ul class="ul-menu">
                            <a href="home.php"><li class="active"> หน้าหลักเว็บไซต์</li></a>
                           <li class="showli"> <a href="course.php">หลักสูตร</a> <span style='font-size:25px; float: right; position: relative; right: 10px;'>&or;</span> <ul class="dropdown">
                                <a href="professor.php"><li>คณะอาจารย์</li></a>
                                <a href="award.php"><li >ผลงานและรางวัล</li></a></li>
                            </ul>
                            <a href="Yearbook.php"><li >ทำเนียบรุ่น</li></a>
                            <a href="activity.php"><li> ภาพกิจกรรม</li></a>
                            <a href="QA.php"><li >คำถาม QA</li></a>
                            <a href="contact.php"><li>การติดต่อ</li></a>
                           <a href="massage.php"><li>กล่องข้อความ</li></a>
                            <a href="update_admin.php"><li>Admin</li></a>
                          </ul>
            </div>
        </div>
        <!-- End head -->
        <!-- Start Content -->
        <div class="content">
            <div class="head-text">
                <h1>ข่าวสาร</h1>
            </div>
            <div class="content-inside">
                <div class="text-head">   
                    <form action="update_news.php" method="POST" enctype="multipart/form-data">
               
            </div>
            <div class="content-inside">
            <div class="text-head"> <span class="span-topic">หัวข้อ : </span> <input type="text" name="toppic" id="input-topic"onkeyup="countTextJs1()" maxlength="70" placeholder="ชื่อเรื่อง..."><br><p > ความยาวไม่เกิน 70 ตัวอักษร <span id='rs_txtForJs1'></span>/70</p>
                <br>
              <span  class="span-topic">วันที่ลงประกาศ : </span>  <input type="date" name="date" id="input-data"> <br>
                    <div class="status"><span  class="span-topic"> สถานะการเผยแพร่ : </span>  <input type="radio" name="status" id="radio-on" value="1" checked> <label for="radio-on">เผยแพร่</label> 
                        <input type="radio" name="status" id="radio-off" value="0" > <label for="radio-off">ไม่เผยแพร่</label><br>
                    </div> 
                    <div class="uploadimg"> 
                        
                    <span  class="span-topic">รูปภาพหลัก : </span>
                     <input type="file" name="main"  class="btn-upload">
                  <p class='textred'>ต้องใส่รูปภาพอย่างน้อย 1 รูปภาพ</p>
                     <br>
                       
                    <!-- <span  class="span-topic">รูปภาพกิจกรรม : </span> <input type="file" name="activity"  class="btn-upload">     -->
                    </div> 
               <div class="div-textarea"> <span  class="span-topic">รายละเอียด </span> <textarea name="description" id="" cols="20" rows="10"></textarea> </div> 
               <div class="Gbtn"> <input type="submit"name="btn-ok" value="บันทึก" class="btn-ok">
                <a href="home.php"><input type="button" value="ย้อนกลับ" class="btn-back"></div></a>
                </div>

            </div>
            </form>

            </div>
        </div>
    <footer>
        <div class="foot">
        </div>
    </footer>
    <script>
        CKEDITOR.replace( 'description' );
</script>
<script type="text/javascript">
 function countTextJs1(){//ฟังก์ชั่นนับจำนวนตัวอักษรรวมช่องว่าง
  var txtForJs1=document.getElementById('input-topic').value;
  var countTxt=txtForJs1.length;
    document.getElementById('rs_txtForJs1').innerHTML=countTxt
 }

</script>
</body>
</html>